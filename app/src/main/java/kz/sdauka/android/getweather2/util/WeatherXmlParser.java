package kz.sdauka.android.getweather2.util;

import android.content.Context;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import kz.sdauka.android.getweather2.model.Forecast;
import kz.sdauka.android.getweather2.model.Location;
import kz.sdauka.android.getweather2.model.Weather;

/**
 * Created by Dauletkhan on 18.02.2015.
 */
public class WeatherXmlParser {

    public static Weather getWeather(String data, Context context) {
        Weather weathers = new Weather();
        Location location = new Location();
        List<Forecast> timeList = new ArrayList<>();
        try {
            XmlPullParserFactory xppf = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xppf.newPullParser();
            InputStream input = new ByteArrayInputStream(data.getBytes());
            parser.setInput(input, "utf-8");
            Forecast forecast = new Forecast();
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {

                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("name")) {
                    location.setCity(parser.nextText());
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("country")) {
                    location.setCountry(parser.nextText());
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("time")) {

                    forecast.setDay(parser.getAttributeValue(0));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("symbol")) {
                    forecast.setDescription(parser.getAttributeValue(1));
                    forecast.setIcon(parser.getAttributeValue(2));
                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("windSpeed")) {
                    forecast.setWidnSpeed(Float.parseFloat(parser.getAttributeValue(0)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("temperature")) {
                    forecast.setMaxTemp(Float.parseFloat(parser.getAttributeValue(1)));
                    forecast.setMinTemp(Float.parseFloat(parser.getAttributeValue(2)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("pressure")) {
                    forecast.setPressure(Float.parseFloat(parser.getAttributeValue(1)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("humidity")) {
                    forecast.setHumidity(Float.parseFloat(parser.getAttributeValue(0)));

                }
                if (parser.getEventType() == XmlPullParser.START_TAG
                        && parser.getName().equals("clouds")) {
                    forecast.setCloudPerc(Integer.parseInt(parser.getAttributeValue(1)));
                    timeList.add(forecast);
                    forecast = new Forecast();
                }
                parser.next();
            }
            weathers.setLocation(location);
            weathers.setForecast(timeList);
        } catch (Throwable t) {
            Toast.makeText(context,
                    "Ошибка при загрузке XML-документа: " + t.toString(), Toast.LENGTH_SHORT)
                    .show();
        }
        return weathers;
    }
}
