package kz.sdauka.android.getweather2.module.forecast;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import kz.sdauka.android.getweather2.R;
import kz.sdauka.android.getweather2.model.Weather;

/**
 * Created by Dauletkhan on 22.02.2015.
 */
public class ForecastFragment extends Fragment {
    private Weather weather;
    private int page;
    @InjectView(R.id.cityName)
    TextView cityName;
    @InjectView(R.id.description)
    TextView description;
    @InjectView(R.id.temp)
    TextView temp;
    @InjectView(R.id.wind)
    TextView windSpeed;
    @InjectView(R.id.pressure)
    TextView pressure;
    @InjectView(R.id.humidity)
    TextView humitidy;
    @InjectView(R.id.day)
    TextView day;
    @InjectView(R.id.imageView)
    ImageView imageView;

    public ForecastFragment() {
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public static ForecastFragment newInstance(int page, Weather weather) {
        ForecastFragment fragment = new ForecastFragment();
        fragment.setPage(page);
        fragment.setWeather(weather);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_forecast, container, false);
        ButterKnife.inject(this, rootView);
        cityName.setText(weather.getLocation().getCity() + ", " + weather.getLocation().getCountry());
        description.setText(weather.getForecast().get(page - 1).getDescription());
        temp.setText((int) weather.getForecast().get(page - 1).getMaxTemp() + "° / " + (int) weather.getForecast().get(page - 1).getMinTemp() + "°");
        windSpeed.setText((int) weather.getForecast().get(page - 1).getWidnSpeed() + "m/s");
        pressure.setText((int) weather.getForecast().get(page - 1).getPressure() + "hPa");
        humitidy.setText((int) weather.getForecast().get(page - 1).getHumidity() + "%");
        day.setText(weather.getForecast().get(page - 1).getDay());
        getImage();
        return rootView;
    }

    private ImageView getImage() {
        switch (weather.getForecast().get(page - 1).getIcon()) {
            case "01d":
                imageView.setImageResource(R.drawable.d01);
                break;
            case "02d":
                imageView.setImageResource(R.drawable.d02);
                break;
            case "03d":
                imageView.setImageResource(R.drawable.d03);
                break;
            case "04d":
                imageView.setImageResource(R.drawable.d04);
                break;
            case "09d":
                imageView.setImageResource(R.drawable.d09);
                break;
            case "10d":
                imageView.setImageResource(R.drawable.d10);
                break;
            case "11d":
                imageView.setImageResource(R.drawable.d11);
                break;
            case "13d":
                imageView.setImageResource(R.drawable.d13);
                break;
            case "50d":
                imageView.setImageResource(R.drawable.d50);
                break;
            case "01n":
                imageView.setImageResource(R.drawable.n01);
                break;
            case "02n":
                imageView.setImageResource(R.drawable.n02);
                break;
            case "03n":
                imageView.setImageResource(R.drawable.n03);
                break;
            case "04n":
                imageView.setImageResource(R.drawable.n04);
                break;
            case "09n":
                imageView.setImageResource(R.drawable.n09);
                break;
            case "10n":
                imageView.setImageResource(R.drawable.n10);
                break;
            case "11n":
                imageView.setImageResource(R.drawable.n11);
                break;
            case "13n":
                imageView.setImageResource(R.drawable.n13);
                break;
            case "50n":
                imageView.setImageResource(R.drawable.n50);
                break;
            default:
                break;
        }
        return imageView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
