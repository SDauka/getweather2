package kz.sdauka.android.getweather2.util;

import android.app.Activity;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by sdauka on 26.02.15.
 */
public class Utils {
    public static void setLocale(Activity a, String lang) {
        Locale locale2 = new Locale(lang);
        Locale.setDefault(locale2);
        Configuration c = new Configuration(a.getBaseContext().getResources().getConfiguration());
        c.locale = locale2;
        a.getBaseContext().getResources().updateConfiguration(c, a.getBaseContext().getResources().getDisplayMetrics());
    }
}
