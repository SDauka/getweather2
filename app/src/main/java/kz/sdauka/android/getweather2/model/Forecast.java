package kz.sdauka.android.getweather2.model;

/**
 * Created by Dauletkhan on 18.02.2015.
 */
public class Forecast {
    private String day;
    private float widnSpeed;
    private int cloudPerc;
    private float minTemp;
    private float maxTemp;
    private String description;
    private String icon;
    private float pressure;
    private float humidity;

    public Forecast() {
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public float getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(float minTemp) {
        this.minTemp = minTemp;
    }

    public float getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(float maxTemp) {
        this.maxTemp = maxTemp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getWidnSpeed() {
        return widnSpeed;
    }

    public void setWidnSpeed(float widnSpeed) {
        this.widnSpeed = widnSpeed;
    }

    public int getCloudPerc() {
        return cloudPerc;
    }

    public void setCloudPerc(int cloudPerc) {
        this.cloudPerc = cloudPerc;
    }
}
