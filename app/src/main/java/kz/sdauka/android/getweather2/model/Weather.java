package kz.sdauka.android.getweather2.model;

import java.util.List;

/**
 * Created by Dauletkhan on 14.02.2015.
 */
public class Weather {
    private Location location;
    private List<Forecast> forecast;

    public Weather() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(List<Forecast> forecast) {
        this.forecast = forecast;
    }
}
