package kz.sdauka.android.getweather2.module.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

import kz.sdauka.android.getweather2.R;
import kz.sdauka.android.getweather2.util.Utils;

/**
 * Created by Dauletkhan on 22.02.2015.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    CheckBoxPreference gpsOn;
    EditTextPreference city_name;
    SharedPreferences prefse;

    public SettingsFragment() {
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
        prefse = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
        gpsOn = (CheckBoxPreference) findPreference("gpsOn");
        city_name = (EditTextPreference) findPreference("city_name");
        city_name.setEnabled(!gpsOn.isChecked());
        gpsOn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                city_name.setEnabled(!gpsOn.isChecked());
                return false;
            }
        });


        city_name.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(
                    android.preference.Preference preference, Object newValue) {
                if (newValue.toString().trim().equals("")) {

                    Toast.makeText(getActivity(), "City name can not be empty",
                            Toast.LENGTH_LONG).show();

                    return false;
                }
                return true;
            }
        });
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        String lang = prefse.getString(key, "ru");
        Utils.setLocale(this.getActivity(), lang);
    }
}
