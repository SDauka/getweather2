package kz.sdauka.android.getweather2.module.about;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import kz.sdauka.android.getweather2.R;

/**
 * Created by Dauletkhan on 22.02.2015.
 */
public class AboutFragment extends Fragment {

    public AboutFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        setHasOptionsMenu(false);
        return rootView;
    }
}
