package kz.sdauka.android.getweather2.module.forecast.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import kz.sdauka.android.getweather2.model.Weather;
import kz.sdauka.android.getweather2.module.forecast.ForecastFragment;

/**
 * Created by Dauletkhan on 25.02.2015.
 */
public class ForecastViewPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 7;
    private Weather weather;

    public ForecastViewPagerAdapter(FragmentManager fm, Weather weather) {
        super(fm);
        this.weather = weather;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ForecastFragment.newInstance(1, weather);
            case 1:
                return ForecastFragment.newInstance(2, weather);
            case 2:
                return ForecastFragment.newInstance(3, weather);
            case 3:
                return ForecastFragment.newInstance(4, weather);
            case 4:
                return ForecastFragment.newInstance(5, weather);
            case 5:
                return ForecastFragment.newInstance(6, weather);
            case 6:
                return ForecastFragment.newInstance(7, weather);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }


}
