package kz.sdauka.android.getweather2.module.forecast;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import kz.sdauka.android.getweather2.R;
import kz.sdauka.android.getweather2.model.Weather;
import kz.sdauka.android.getweather2.module.forecast.adapter.ForecastViewPagerAdapter;
import kz.sdauka.android.getweather2.util.GpsTracker;
import kz.sdauka.android.getweather2.util.WeatherXmlParser;

/**
 * Created by Dauletkhan on 25.02.2015.
 */
public class ForecastViewPager extends Fragment {
    private FragmentActivity myContext;
    private Weather weather;
    private ProgressDialog progressDialog;
    private GpsTracker gps;
    FragmentPagerAdapter adapterViewPager;
    @InjectView(R.id.pager)
    ViewPager viewPager;
    private boolean gpsOn;
    private String cityName;

    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_view_pager, null);
        ButterKnife.inject(this, rootView);
        setHasOptionsMenu(true);

        progressDialog = new ProgressDialog(myContext);
        progressDialog.setCancelable(false);
        checkPref();
        return rootView;
    }

    private void checkPref() {
        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        gpsOn = myPref.getBoolean("gpsOn", true);
        if (!gpsOn) {
            cityName = myPref.getString("city_name", "");
            if (cityName.isEmpty()) {
                Toast.makeText(getActivity().getApplicationContext(), "Укажите город в настройках", Toast.LENGTH_SHORT);
            } else {
                prepareWeather();
            }
        } else {

            prepareWeather();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        checkPref();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        checkPref();
        super.onResume();
    }

    private void prepareWeather() {
        gps = new GpsTracker(getActivity());
        RequestParams params = new RequestParams();
        if (gpsOn) {
            if (gps.canGetLocation()) {
                params.put("lat", gps.getLatitude());
                params.put("lon", gps.getLongitude());
                params.put("mode", "xml");
                params.put("units", "metric");
                params.put("cnt", 7);
                getWeather(params, progressDialog);
            } else {
                gps.showSettingsAlert();
            }
        } else {
            params.put("q", cityName);
            params.put("mode", "xml");
            params.put("units", "metric");
            params.put("cnt", 7);
            getWeather(params, progressDialog);
        }
    }

    private void getWeather(RequestParams params, final ProgressDialog progressDialog) {
        progressDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.openweathermap.org/data/2.5/forecast/daily", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressDialog.hide();
                try {
                    String data = new String(responseBody, "UTF-8");
                    weather = WeatherXmlParser.getWeather(data, myContext);
                    adapterViewPager = new ForecastViewPagerAdapter(myContext.getSupportFragmentManager(), weather);
                    viewPager.setAdapter(adapterViewPager);
                } catch (UnsupportedEncodingException e) {
                    Toast.makeText(myContext, "Произошла ошибка [XML ответ от сервера может быть недействительным]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressDialog.hide();
                // Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(myContext, "Запрашиваемый город не найден", Toast.LENGTH_LONG).show();
                }
                // Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(myContext, "Что-то пошло не так на сервере", Toast.LENGTH_LONG).show();
                }
                //Http response code other than 404, 500
                else {
                    Toast.makeText(myContext, "Произошла ошибка! [Наиболее распространенные ошибки: Устройство не подключено к Интернету или удаленный сервер не запущен]", Toast.LENGTH_LONG).show();
                }
            }

        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_update:
                prepareWeather();
                Toast.makeText(myContext, "Updated", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStop() {
        gps.stopUsingGPS();
        progressDialog.dismiss();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        gps.stopUsingGPS();
        progressDialog.dismiss();
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    public void onDestroyView() {
        gps.stopUsingGPS();
        progressDialog.dismiss();
        super.onDestroy();
        ButterKnife.reset(this);
    }
}
